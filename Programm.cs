﻿using NAND_Prog;
using NAND_Prog.UILib;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageProgram_80h_10h
{
    [Export(typeof(Operation)),
        ExportMetadata("Name", "PageProgram_80h_10h")]
    public class Programm : AbstractProgramm
    {

        public Programm()
        {
            name = "Programm";
            based = typeof(Page);

            //    optBox.canVerBySR = true;                 
            optBox.AddOption(new VerifyBySR());         // результат виконання цієї операції можна(це опція) перевіряти на стороні самого чіпа , первіряючи статус-регістр
            optBox.AddOption(new UseSpareArea());       // Можна(це опція) писати додатково додаткову область

            Instruction instruction;
            //--------------------------------------------------------------
            instruction = new WriteCommand();                              //Створюю інструкцію WriteCommand
            chainInstructions.Add(instruction);                            //Додаю її в ланцюжок інструкцій операції ReadPage

            //    instruction.numberOfcycles = 0x01;                       //По дефолту 1 
            (instruction as WriteCommand).command = 0x80;
            (instruction as WriteCommand).Implementation = GetCommandMG;
            //--------------------------------------------------------------
            //--------------------------------------------------------------

            instruction = new WriteAddress();
            chainInstructions.Add(instruction);

            instruction.numberOfcycles = (Chip.memOrg.colAdrCycles + Chip.memOrg.rowAdrCycles); // /*(5Cycle)*/ Адрес буде підставляти кожна сторінка свій
            (instruction as WriteAddress).Implementation = GetAddressMG;
            //--------------------------------------------------------------
            //--------------------------------------------------------------

            instruction = new WriteData();
            chainInstructions.Add(instruction);
            (instruction as WriteData).Implementation = GetDataMG;

            //--------------------------------------------------------------
            //--------------------------------------------------------------
            instruction = new WriteCommand();                              //Створюю інструкцію WriteCommand
            chainInstructions.Add(instruction);                            //Додаю її в ланцюжок інструкцій операції ReadPage

            //    instruction.numberOfcycles = 0x01;                        //По дефолту 1   
            (instruction as WriteCommand).command = 0x10;
            (instruction as WriteCommand).Implementation = GetCommandMG;
            //--------------------------------------------------------------

        }

        private BufMG GetDataMG(ChipPart client)                        //Реалізація _implementation для  WriteData : Instruction
        {
            DataMG meneger = new DataMG();
            meneger.direction = Direction.Out;     //дані на вихід

            meneger.SetDataPlace((client as IDatable).GetDataPlace());
            //Вираховую початок сторінки в файлі Chip.chipprovider.mmf (з врахуванням column)
            //meneger.dataOffset = ((client as Page).address >> (Chip.memOrg.colAdrCycles * 8))  // ‭0x1FFFF0000‬  (‭0001 1111 1111 1111 1111 0000 0000 0000 0000‬) --> 0x1FFFF (‭0001 1111 1111 1111 1111‬)
            //                         *
            //                         (ulong)((client as Page).Size())                  // 2112 байт
            //                         +
            //                         (client as Page).column;                                           // додатково зміщуюсь на відповідний стовбець , якщо 0 -то стаю на початок сторінки

            meneger.dataOffset = (ulong)(client as Page).OffSet()
                                 +
                                 (client as Page).column;                                           // додатково зміщуюсь на відповідний стовбець , якщо 0 -то стаю на початок сторінки

            //Вираховую розмір даних для читання в файл Chip.chipprovider.mmf (з врахуванням column)
            //Треба вияснити чи можна писати в SpareArea!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            int x;
            //Тут я повинен звернутись до optBox і подивитись чи користувач перед запуском оперції поставив галочку optBox.needSpareArea
            //і на основі цієї інформації і client._size + client._spareArea_size - client.column вирахувати розмір даних


            if (this.optBox.IsCheckedOption(typeof(UseSpareArea)))
                meneger.numberOfCycles = (int)((client as Page).FullSize() - (client as Page).column);               // 2112 байт
            else
                meneger.numberOfCycles = (int)((client as Page).FullSize() - (client as Page).spareArea_size - (client as Page).column);                                                  //2048 bytes


            return meneger;
        }



    }
}
